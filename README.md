# jcommanderStarter

## 介绍
java 命令行工具开发微型架子

jcommander的微型启动器, 提供了快捷的启动方式, 通用异常处理, -h的帮助打印, 参数校验等.

#### 博客链接
[使用方式详情介绍博客](https://www.yuque.com/docs/share/248a82ca-1f22-4058-a0a6-7d31a3c75a66?#%20%E3%80%8Ajava%20%E5%BC%80%E5%8F%91%E5%91%BD%E4%BB%A4%E8%A1%8C%E5%B0%8F%E5%B7%A5%E5%85%B7%E3%80%8B)

#### 版本介绍:
##### 1.0.1-SNAPSHOT
2023年4月7日, 更新到版本1.0.1-SNAPSHOT,
- 包名更换, 因为那个域名到期了。换了个小时候看动漫后给自己憧憬的理想国取的一个名字。
- 放弃了jcommandar自带的参数校验, 校验环节完全交给jsr的validator。
- 对文档进行了中国本地化的处理， BaseParameter接口要求必填程序命名和程序描述。
- 对BaseParameter下的枚举类型,可以让枚举实现ParameterEnum接口, 它可以在文档上打印出枚举的原本字符串和描述字符。

下面是一个参数类及其附带的枚举类型的java源代码,再提供其在帮助文档打印:
```java
/**
 * 参数类
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AutoValidClose
public class Parameters extends BaseParameter {

      @Parameter(names = {"-m", "-mode"}, description = "模式", order = 1, required = true)
      @NotNull
      private Mode mode = Mode.backup;

      @Parameter(names = {"-s", "-srcPath"}, description = "目标地的文件名或目录名,比如从A复制到B, 这里填A的路径", order = 2)
      private String srcPath;
      @Parameter(names = {"-t", "-targetPath"}, description = "复制目的地的文件名或者目录名, 比如从A复制到B, 这里填B的路径", order = 3)
      private String targetPath;
      @Parameter(names = {"-b", "-backupDirectory"}, description = "备份目录, 比如从A复制到B, 这里填备份B和A冲突的文件的路径", order = 4)
      private String backupDirectory;

      @Parameter(names = {"-cl", "-copyLogPath"}, description = "复制日志路径, 在copy操作下, 默认为./, 还原操作为null", order = 5)
      private String copyLogPath;
      @Parameter(names = {"-rl",
          "-recoveryLogPath"}, description = "还原日志路径, 默认./xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", order = 6)
      private String recoveryLogPath = "./";

      @Override
      public String getSoftwareDescription() {
            return "测试软件, 一切为了测试";
      }

      @Override
      public String getSoftwareName() {
            return "测试";
      }
}

/**
 * 参数附带的枚举类
 */
public enum Mode implements ParameterEnum {
      copy("复制模式"), recovery("恢复模式"), backup("备份模式");
      private final String memo;

      Mode(String memo) {
            this.memo = memo;
      }

      @Override
      public String getDescription() {
            return memo;
      }
}
```
```cmd
程序《测试》说明文档
	文档说明: 参数"-m, -mode [必填][无值参数]"就表示要在命令行输入"-m"后面跟空格和参数值, ",-mode"表示这
个参数也可以写成"-mode",他和"-m"是等效的, [必填]就是参数必填, [无值参数]表示写出参数名就可以了,不要值的。
	概览: 测试软件, 一切为了测试
	参数:
		-m, -mode [必填]
			描述: 模式
			默认: backup
			允许的值: copy: 复制模式,  recovery: 恢复模式,  backup: 备份模式  
		-s, -srcPath 
			描述: 目标地的文件名或目录名,比如从A复制到B, 这里填A的路径
		-t, -targetPath 
			描述: 复制目的地的文件名或者目录名, 比如从A复制到B, 这里填B的路径
		-b, -backupDirectory 
			描述: 备份目录, 比如从A复制到B, 这里填备份B和A冲突的文件的路径
		-cl, -copyLogPath 
			描述: 复制日志路径, 在copy操作下, 默认为./, 还原操作为null
		-rl, -recoveryLogPath 
			描述: 还原日志路径, 默认./xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxx
			默认: ./
		-help, --help, --h, -h [无值参数]
			描述: 帮助
```