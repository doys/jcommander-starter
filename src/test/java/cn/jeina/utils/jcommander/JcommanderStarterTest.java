package cn.jeina.utils.jcommander;

import cn.hutool.core.util.ReflectUtil;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @version 1.0
 * @description
 * @author zhyu
 * 2022-9-23 15:49
 */
class JcommanderStarterTest {

    @Test
    void test() throws InvocationTargetException, IllegalAccessException {
        String url = "Was passed main parameter '-c' but no main parameter was defined in your arg class";
        JcommanderStarter jcommanderStarter = new JcommanderStarter();
        Method method = ReflectUtil.getMethod(JcommanderStarter.class, "parseErrorParamName", String.class);
        method.setAccessible(true);
        Object invoke = method.invoke(jcommanderStarter, url);
        assert "-c".equals(invoke);
    }

    @Test
    void test2(){
        String[] args = new String[]{"-h"};
        JcommanderStarter.start(args);
    }

}