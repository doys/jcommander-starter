package cn.jeina.utils.jcommander.demo;

import cn.jeina.utils.jcommander.BaseParameter;
import com.beust.jcommander.Parameter;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhyu
 * @version 1.0
 * @date 2022-7-31 9:12
 */
@Data
public class MyParams extends BaseParameter {

    @NotNull
    @Parameter(names = {"-c"}, description = "颜色", required = true)
    private Color color;
    @Parameter(names = {"-p"}, description = "电话", required = true)
    private String phone;
    private int code;
    private double score;

    @Override
    public String getSoftwareDescription() {
        return "测试软件, 一切为了测试";
    }

    @Override
    public String getSoftwareName() {
        return "测试";
    }
}
