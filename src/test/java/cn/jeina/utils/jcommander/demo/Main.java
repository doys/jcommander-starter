package cn.jeina.utils.jcommander.demo;

import cn.jeina.utils.jcommander.JcommanderStarter;

/**
 * @author zhyu
 * @version 1.0
 * @date 2022-7-31 9:16
 */
public class Main {
    public static void main(String[] args) {
        JcommanderStarter.start(args);
    }
}
