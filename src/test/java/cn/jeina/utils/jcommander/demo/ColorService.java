package cn.jeina.utils.jcommander.demo;

import cn.jeina.utils.jcommander.JcommanderService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zhyu
 * @version 1.0
 * @date 2022-7-31 9:16
 */
@Slf4j
public class ColorService implements JcommanderService<MyParams> {
    @Override
    public void execute(MyParams baseParameter) {
        log.info("执行到成功:{}", baseParameter);
    }
}
