package cn.jeina.utils.jcommander.demo2;

import cn.jeina.utils.jcommander.ParameterEnum;

/**
 * @author zhyu 2022-9-23 8:49
 * @version 1.0
 * @description
 */
public enum Mode implements ParameterEnum {
      copy("复制模式"), recovery("恢复模式"), backup("备份模式");
      private final String memo;

      Mode(String memo) {
            this.memo = memo;
      }

      @Override
      public String getDescription() {
            return memo;
      }
}
