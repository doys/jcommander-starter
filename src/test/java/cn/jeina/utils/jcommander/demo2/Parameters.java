package cn.jeina.utils.jcommander.demo2;

import cn.jeina.utils.jcommander.BaseParameter;
import cn.jeina.utils.validator.AutoValidClose;
import com.beust.jcommander.Parameter;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhyu
 * @version 1.0
 * @date 2022-7-15 9:27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AutoValidClose
public class Parameters extends BaseParameter {

      @Parameter(names = {"-m", "-mode"}, description = "模式", order = 1, required = true)
      @NotNull
      private Mode mode = Mode.backup;

      @Parameter(names = {"-s", "-srcPath"}, description = "目标地的文件名或目录名,比如从A复制到B, 这里填A的路径", order = 2)
      private String srcPath;
      @Parameter(names = {"-t", "-targetPath"}, description = "复制目的地的文件名或者目录名, 比如从A复制到B, 这里填B的路径", order = 3)
      private String targetPath;
      @Parameter(names = {"-b", "-backupDirectory"}, description = "备份目录, 比如从A复制到B, 这里填备份B和A冲突的文件的路径", order = 4)
      private String backupDirectory;

      @Parameter(names = {"-cl", "-copyLogPath"}, description = "复制日志路径, 在copy操作下, 默认为./, 还原操作为null", order = 5)
      private String copyLogPath;
      @Parameter(names = {"-rl",
          "-recoveryLogPath"}, description = "还原日志路径, 默认./xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", order = 6)
      private String recoveryLogPath = "./";

      @Override
      public String getSoftwareDescription() {
            return "测试软件, 一切为了测试";
      }

      @Override
      public String getSoftwareName() {
            return "测试";
      }
}
