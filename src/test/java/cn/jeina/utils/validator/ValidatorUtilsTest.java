package cn.jeina.utils.validator;

import lombok.Data;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @description
 * @author zhyu
 * 2022-9-23 14:39
 */
class ValidatorUtilsTest {
    interface M {}
    @Data
    class Model{
        @Min(value = 20, groups = M.class)
        private int a;

        public Model(int a) {
            this.a = a;
        }
    }
    @Test
    void validAllList() {
        List<Model> list = new ArrayList<>();
        list.add(new Model(1));
        list.add(new Model(2));
        list.add(new Model(23));
        list.add(new Model(444));
        ValidatorUtils.validAllList(list);
        ValidatorUtils.validAllList(list, M.class);

    }
}