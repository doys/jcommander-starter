package cn.jeina.utils.validator;

/**
 * 自动关闭校验
 * @version 1.0
 * @description
 * @author zhyu
 * 2022-9-23 11:23
 */
public @interface AutoValidClose {
}
