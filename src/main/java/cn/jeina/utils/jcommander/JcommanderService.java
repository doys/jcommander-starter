package cn.jeina.utils.jcommander;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 提供具体服务的service 实现类必须提供参数的泛型信息 这里是spi机制 需要在classpath 下面创建文件夹META-INF.services 在里面创建文件 cn.changeforyou.utils.jcommander.JcommanderService
 * 文件的内容是该服务实现类的完全类名比如cn.changeforyou.knowledge.yuque2local.JcommanderServiceImpl 可以参考demo
 *
 * @param <T>
 */
public interface JcommanderService<T extends BaseParameter> {

      /**
       * 具体执行方法
       */
      void execute(T baseParameter);

      static Logger log = LoggerFactory.getLogger(JcommanderService.class);

      default void execute_do(T baseParameter) {
            log.info("《{}》开始执行", baseParameter.getSoftwareName());
            execute(baseParameter);
            log.info("《{}》执行完毕", baseParameter.getSoftwareName());
      }

      /**
       * 执行校验前的方法
       */
      default void beforeValidate() {
      }
}
