package cn.jeina.utils.jcommander;

/**
 * @author zhyu 2023-4-7 14:18
 * @version 1.0
 * @description
 */
public interface ParameterEnum {

      /**
       * 获取参参数(枚举)的描述
       */
      String getDescription();
}
