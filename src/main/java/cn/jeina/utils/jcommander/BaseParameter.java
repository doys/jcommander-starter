package cn.jeina.utils.jcommander;

import com.beust.jcommander.Parameter;
import lombok.Data;

/**
 * 该基础实体支持java的validation 比如@NotBlank @Max(2)等注解
 * 具体可以查看<a href="http://jcommander.org/#_overview">jcommander官网</a>
 * 一般来说使用 {@link Parameter}注解, 注解填上names和description就可以了 required别填, 因为完全不如java的validation API好使, 所以用它来代替
 *
 * @author zhyu
 * @version 1.0
 * @date 2022-7-30 22:11
 */
@Data
public abstract class BaseParameter {
    @Parameter(names = {"-help", "--help", "--h", "-h"}, help = true, description = "帮助")
    private boolean help;

    public abstract String getSoftwareDescription();
    public abstract String getSoftwareName();
}
